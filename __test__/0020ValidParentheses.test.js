const isValid = require('../src/0020ValidParentheses');

describe("first test",()=>{
    it("values () must return true", () =>{
        const result = isValid("()")
        expect(result).toBeTruthy()
    }),
    it("values ()[]{} must return true", () =>{
        const result = isValid("()[]{}")
        expect(result).toBeTruthy()
    }),
    it("values (] must return false", () =>{
        const result = isValid("(]")
        expect(result).toBeFalsy()
    })
});