const { mergeTwoLists, ListNode } = require("../src/0021MergeTwoSortedList");

function createListNode(){
  if (!arguments) return null;
  let list = new ListNode(0, null);
  let current = list;
  //let arr = Array.prototype.slice.call(arguments);
  for (const n of arguments) {
    current.next = new ListNode(n, null);
    current = current.next;
  }
  return list.next;
}

describe("Merge two sorted list", () => {
  it("values null, null must return null", () => {
    const list1 = createListNode();
    const list2 = createListNode();
    const result = mergeTwoLists(list1, list2);
    expect(result).toBeNull();
  }),
    it("values [1,2,4], [1,3,4] must return [1,1,2,3,4,4]", () => {
      const list1 = createListNode(1, 2, 4);
      const list2 = createListNode(1, 3, 4);
      const total = createListNode(1, 1, 2, 3, 4, 4);
      const result = mergeTwoLists(list1, list2);
      
      expect(result).toStrictEqual(total);
    }),
    it("values null, [0] must return [0]", () => {
      const list1 = createListNode();
      const list2 = createListNode(0);
      const total = createListNode(0);
      const result = mergeTwoLists(list1, list2);
      
      expect(result).toStrictEqual(total);
    })
});
