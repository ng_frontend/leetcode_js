/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */

// eslint-disable-next-line no-unused-vars
function ListNode (val, next) {
  this.val = (val === undefined ? 0 : val)
  this.next = (next === undefined ? null : next)
}
/**
 * @param {ListNode} list1
 * @param {ListNode} list2
 * @return {ListNode}
 */
const mergeTwoLists = function (list1, list2) {
  let total = new ListNode(0,null)
  let current = total

  while(list1 && list2)
  {
    if(list1.val < list2.val)
    {
      current.next = list1
      list1 = list1.next
    }
    else{
      current.next = list2
      list2 = list2.next
    }
    current = current.next
  }
  if (!list1) { current.next = list2 } else if (!list2) { current.next = list1 }
  return total.next
}

/*
const test = function(){
  let list = new ListNode(12, null)
  let list2 = new ListNode(11,list)
  let arr = []
  console.log(list2)
  while(list2)
  {
    arr.push(list2.val)
    list2 = list2.next
  }
  console.log(arr)
  
}
*/

module.exports = {mergeTwoLists,ListNode}
