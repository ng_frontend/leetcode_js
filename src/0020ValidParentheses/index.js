/**
 * @param {string} s
 * @return {boolean}
 */
var isValid = function (s) {
  let stack = [];
  for (let ch of s) {
    if (ch === "(" || ch === "[" || ch === "{") {
      stack.push(ch);
    } else {
      let val = stack.pop();
      if (
        (val === "(" && ch === ")") ||
        (val === "[" && ch === "]") ||
        (val === "{" && ch === "}")
      ) {
        continue;
      }
      return false;
    }
  }
  return !(stack.length > 0);
};
module.exports = isValid;
